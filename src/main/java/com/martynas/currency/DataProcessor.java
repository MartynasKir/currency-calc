package com.martynas.currency;


import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class DataProcessor {
    private String urlRaw;
    private Document document;


    public DataProcessor(String url){
        this.urlRaw = url;

        this.document = null;
    }


    public String getUrlRaw() {
        return urlRaw;
    }


    public Document download (){

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();

            URL url = new URL(urlRaw);
            InputStream stream = url.openStream(); // return stream to read file
            Document document = docBuilder.parse(stream);
            return document;
        } catch (Exception e){
            System.out.println("An exception occurred: \n" + e);
        }
        return document;
    }


    public List getDates(Document document){
        String expr = "//item/data/text()";
        return getList(expr, document);
    }


    public List getRates (Document document){
        String expr = "//item/santykis/text()";
        return getList(expr, document);
    }


    private List getList (String expr, Document doc){
        List<String> list = new ArrayList<>();
        try {

            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList result = (NodeList)xPath.compile(expr).evaluate(doc, XPathConstants.NODESET);

            for (int i = 0; i < result.getLength(); i++)
                list.add(result.item(i).getNodeValue());
            return list;

        } catch (Exception e){
            System.out.println("An exception occurred: \n" + e);
        }
        return list;
    }


    public void showData (List datesList, List ratesList){
        for (int i = 0; i < ratesList.size(); i++){
            System.out.println(datesList.get(i) + " : " + ratesList.get(i));
        }
    }


    public Double currency_delta(List ratesList) {
        double rate_start = Double.parseDouble(ratesList.get(ratesList.size() - 1)
                .toString()
                .replace(',', '.'));
        double rate_end = Double.parseDouble(ratesList.get(1)
                .toString()
                .replace(',', '.'));

        double rate_diff = rate_end - rate_start;

        return rate_diff;
    }

}


