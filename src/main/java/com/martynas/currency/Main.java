package com.martynas.currency;

import org.w3c.dom.Document;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		//args: [currency],[start_date],[end_date]

		String url;
		String currency;

		if (args.length != 0) {
			url = "https://www.lb.lt/lt/currency/exportlist/?xml=1&currency=" + args[0] + "&ff=1&class=Eu&type=day&date_from_day=" + args[1] + "&date_to_day=" + args[2];
			currency = "Currency (" + args[0] + ") rates for period: " + args[1] + " to " + args[2];
		} else {
			System.out.println("Running with default arguments. Please specify custom arguments");
			String currencyName = "CZK";
			String startDate = "2019-06-10";
			String endDate = "2019-06-20";
			url = "https://www.lb.lt/lt/currency/exportlist/?xml=1&currency=" + currencyName + "&ff=1&" +
					"class=Eu&type=day&date_from_day=" + startDate + "&date_to_day=" +endDate;
			currency = currencyName + " currency rates: ";
		}

		DataProcessor processor = new DataProcessor(url);
		Document doc = processor.download();
		List datesList = processor.getDates(doc);
		List ratesList = processor.getRates(doc);

		System.out.println(currency);
		processor.showData(datesList,ratesList);

		Double delta = processor.currency_delta(ratesList);
		System.out.println("Currency rate delta for this period is: " + delta);

	}

}
