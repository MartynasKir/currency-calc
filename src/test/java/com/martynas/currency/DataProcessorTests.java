package com.martynas.currency;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

import java.util.List;

public class DataProcessorTests {
    String testUrl;
    DataProcessor processor;
    Document document;

    public DataProcessorTests (){
        this.testUrl = "https://www.lb.lt/lt/currency/exportlist/?xml=1&currency=CZK&ff=1&class=Eu&type=day&date_from_day=2019-06-20&date_to_day=2019-06-24";
        this.processor = new DataProcessor(testUrl);
        this.document = processor.download();
    }

    @Test
    public void retrunDateValuesFromFile(){
        String [] dates = {"Data","2019-06-24","2019-06-21","2019-06-20"};

        List listGenerated = processor.getDates(document);

        Assert.assertEquals(listGenerated.toArray(), dates);
        System.out.println("Test retrunDateValuesFromFile passed");
    }

    @Test
    public void retrunRateValuesFromFile(){
        String [] rates = {"Santykis","25,601","25,609","25,619"};

        List listGenerated = processor.getRates(document);

        Assert.assertEquals(listGenerated.toArray(), rates);
        System.out.println("Test retrunRateValuesFromFile passed");
    }

    @Test
    public void shouldSubtractStartRateFromEndRateAndReturnResult(){
        List rates = processor.getRates(document);
        Double actual = processor.currency_delta(rates);
        Double expected = -0.018000000000000682;

        Assert.assertEquals(expected,actual);
        System.out.println("Test shouldSubtractStartRateFromEndRateAndReturnResult passed");


    }
}
