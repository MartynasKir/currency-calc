# currency-calc

Project was created to download particular information from website (xml format) and perform calculation on currency rate.

How to run script on Windows?
1. Download project with currency-calc-0.0.1-SNAPSHOT.jar file.
2. Open cmd and type java -jar C:/path/to/file/currency-calc-0.0.1-SNAPSHOT.jar file

Note: A jar is compiled with JDK 11 so if you have older JDK versions please install JDK 11 and specify it at command line:

example: (path to java.exe provided instead of java, no args provided)
 "C:\Program Files\Java\jdk-11.0.1\bin\java.exe" -jar C:\Users\path\to\project\currency-calc\currency-calc-0.0.1-SNAPSHOT.jar

The script will output results with predefined arguments, but you can provide other arguments at the end of the command:

accepted arguments: [currency code] [start_date] [end_date]

example: (additional args: AUD 2019-06-01 2019-06-30)
"C:\Program Files\Java\jdk-11.0.1\bin\java.exe" -jar C:\Users\path\to\project\currency-calc\currency-calc-0.0.1-SNAPSHOT.jar AUD 2019-06-01 2019-06-30


